/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: I-2019
*/

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>

class L1cache : public ::testing::Test{
    protected:
	int debug_on;
	virtual void SetUp()
	{
           /* Parse for debug env variable */
	   get_env_var("TEST_DEBUG", &debug_on);
	};
};

/*
 * TEST1: Verifies miss and hit scenarios for srrip policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_srrip){
  int status;
  int i;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = 0;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug_on) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
 
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (i = 0 ; i < 2; i++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = (associativity <= 2)? rand()%associativity: 3;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     bool(debug_on));
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = srrip_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     (bool)debug_on);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}


/*
 * TEST2: Verifies miss and hit scenarios for lru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_lru) {
  int status;
  int i;
  int a;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = false;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (a = 0 ; a < 2; a++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = i;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)a;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     debug);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = lru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     debug);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }

}

/*
 * TEST3: Verifies miss and hit scenarios for nru policy
 * 1. Choose a random associativity
 * 2. Fill a cache entry
 * 3. Force a miss load
 * 4. Check  miss_hit_status == MISS_LOAD
 * 5. Force a miss store
 * 6. Check miss_hit_status == MISS_STORE
 * 7. Force a hit read
 * 8. Check miss_hit_status == HIT_READ
 * 9. Force a hit store
 * 10. miss_hit_status == HIT_STORE
 */
TEST_F(L1cache, hit_miss_nru) {
  int status;
  int i;
  int a;
  int idx;
  int tag;
  int associativity;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = false;
  operation_result result = {};

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  entry cache_line[associativity];
  /* Check for a miss */
  DEBUG(debug_on,Checking miss operation);
  for (a = 0 ; a < 2; a++){
    /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;
      cache_line[i].rp_value = rand()%2;
      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    /* Load operation for i = 0, store for i =1 */
    loadstore = (bool)a;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     debug);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? MISS_STORE: MISS_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
  /*
   * Check for hit: block was replaced in last iteration, if we used the same 
   * tag now we will get a hit
   */
  DEBUG(debug_on,Checking hit operation);
  for (i = 0 ; i < 2; i++){
    loadstore = (bool)i;
    status = nru_replacement_policy(idx, 
                                     tag, 
                                     associativity,
                                     loadstore,
                                     cache_line,
                                     &result,
                                     debug);
    EXPECT_EQ(status, 0);
    EXPECT_EQ(result.dirty_eviction, 0);
    expected_miss_hit = loadstore ? HIT_STORE: HIT_LOAD;
    EXPECT_EQ(result.miss_hit, expected_miss_hit);
  }
}
/*
 * TEST4: Verifies replacement policy promotion and eviction
 * 1. Choose a random policy 
 * 2. Choose a random associativity
 * 3. Fill a cache entry
 * 4. Insert a new block A
 * 5. Force a hit on A
 * 6. Check rp_value of block A
 * 7. Keep inserting new blocks until A is evicted
 * 8. Check eviction of block A happen after N new blocks were inserted
 * (where N depends of the number of ways)
 */
TEST_F(L1cache, promotion){

  int status;
  int i;
  int a;
  int idx;
  int tag;
  int tag_for_evict;
  int new_block_counter = 0;
  int eviction_number;
  int associativity;
  uint8_t rp_value;
  enum miss_hit_status expected_miss_hit;
  bool loadstore = 1;
  bool debug = true;
  operation_result result = {};

  int policy = rand()%3; //0 for LRU, 1 for NRU, 2 for RRIP
  bool tag_present = true;

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  if (debug) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  entry cache_line[associativity];

  DEBUG(debug_on,Checking promotion);

  /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;

      switch(policy){
        case 0:
          cache_line[i].rp_value = i;
          break;
        case 1:
          cache_line[i].rp_value = 1;
          break;
        case 2:
          cache_line[i].rp_value = (associativity <= 2)? associativity-1: 3;
          break;
        default:
          cache_line[i].rp_value = i;
          break;
      }

      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }
    for(i = 0; i < 2; i++){
      switch(policy) {
        case 0:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 1:
            status = nru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 2:
            status = srrip_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        default:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
      }
    }
    for(i = 0; i <associativity; i++){
      if(cache_line[i].tag == tag && cache_line[i].valid){
        rp_value = cache_line[i].rp_value;
      }
    }
    EXPECT_EQ(unsigned(rp_value), 0);

    //Test for eviction
    DEBUG(debug_on,Checking eviction);

    while(result.evicted_address!=tag){
      tag_for_evict = rand()%4096;
      tag_present = true;
      while(tag_present){
        tag_present = false;
        for(i = 0; i <associativity; i++){
          if(cache_line[i].tag == tag_for_evict && cache_line[i].valid){
            tag_for_evict = rand()%4096;
            tag_present = true;
          }
        }
      }

      switch(policy) {
        case 0:
            status = lru_replacement_policy(idx, 
                                      tag_for_evict, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 1:
            status = nru_replacement_policy(idx, 
                                      tag_for_evict, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 2:
            status = srrip_replacement_policy(idx, 
                                      tag_for_evict, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        default:
            status = lru_replacement_policy(idx, 
                                      tag_for_evict, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
      }

      new_block_counter++;

    }
    if(debug){
      printf("Number of evictions: %d\n", new_block_counter);
    }

    switch(policy) {
        case 0:
            eviction_number = associativity;
            break;
        case 1:
            eviction_number = associativity;
            break;
        case 2:
            eviction_number = (associativity == 2)? 2:(3*(associativity-1)+1);
            break;
        default:
            eviction_number = associativity;
            break;
      }
      if(debug){
        printf("Number of expected evictions: %d\n", eviction_number);
      }

      EXPECT_EQ(new_block_counter, eviction_number);

}


/*
 * TEST5: Verifies evicted lines have the dirty bit set accordantly to the operations
 * performed.
 * 1. Choose a random policy
 * 2. Choose a random associativity
 * 3. Fill a cache entry with only read operations
 * 4. Force a write hit for a random block A
 * 5. Force a read hit for a random block B
 * 6. Force read hit for random block A
 * 7. Insert lines until B is evicted
 * 8. Check dirty_bit for block B is false
 * 9. Insert lines until A is evicted
 * 10. Check dirty bit for block A is true
 */
TEST_F(L1cache, writeback){
  int status;
  int i;
  int idx;
  int tag;
  int tagA;
  int tagB;
  int eviction_dirty_bit;
  int associativity;
  bool loadstore = 1;
  bool debug = true;
  operation_result result = {};

  int policy = rand()%3; //0 for LRU, 1 for NRU, 2 for RRIP

  /* Fill a random cache entry */
  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);
  while(associativity == 1){
    associativity = 1 << (rand()%4);
  }

  if (debug) {
    printf("Entry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  entry cache_line[associativity];

  DEBUG(debug_on,Checking dirty bit);

  /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;

      switch(policy){
        case 0:
          cache_line[i].rp_value = i;
          break;
        case 1:
          cache_line[i].rp_value = 1;
          break;
        case 2:
          cache_line[i].rp_value = (associativity <= 2)? associativity-1: 3;
          break;
        default:
          cache_line[i].rp_value = i;
          break;
      }

      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }

    //Write hit on block A, then read hit on block B, then read hit on A
    loadstore = true;
    tagA = tag;
    while (tag == tagA) {
      tag = rand()%4096;
    }
    tagB = tag;
    tag = tagA;
    for(i = 0; i < 5; i++){
      if(i>1 && i< 4){ //For read hit on B
        loadstore = false;
        tag = tagB;
      } else if(i==4){ //For read hit on A
        tag = tagA;
      }

      switch(policy) {
        case 0:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 1:
            status = nru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 2:
            status = srrip_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        default:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
      }
    }
  //In case of LRU, first eviction should be B. In case of NRU and SRRIP, first eviction should be A
  switch(policy){
    case 0:
      //Force eviction on B
      while(result.evicted_address!=tagB){
        tag = rand()%4096;
        while(tag==tagB){
          tag = rand()%4096;
        }

        switch(policy) {
          case 0:
              status = lru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          case 1:
              status = nru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          case 2:
              status = srrip_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          default:
              status = lru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
        }

    }
    EXPECT_EQ(result.dirty_eviction, false);

    //Force eviction on A
    while(result.evicted_address!=tagA){
        tag = rand()%4096;
        while(tag==tagA){
          tag = rand()%4096;
        }

        switch(policy) {
          case 0:
              status = lru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          case 1:
              status = nru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          case 2:
              status = srrip_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          default:
              status = lru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
        }

      }
      EXPECT_EQ(result.dirty_eviction, true);
      break;
    default:
      //Force eviction on A
      while(result.evicted_address!=tagA){
        tag = rand()%4096;
        while(tag==tagA){
          tag = rand()%4096;
        }

        switch(policy) {
          case 0:
              status = lru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          case 1:
              status = nru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          case 2:
              status = srrip_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          default:
              status = lru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
        }

    }
    EXPECT_EQ(result.dirty_eviction, true);

    //Force eviction on B
    while(result.evicted_address!=tagB){
        tag = rand()%4096;
        while(tag==tagB){
          tag = rand()%4096;
        }

        switch(policy) {
          case 0:
              status = lru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          case 1:
              status = nru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          case 2:
              status = srrip_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
          default:
              status = lru_replacement_policy(idx, 
                                        tag, 
                                        associativity,
                                        loadstore,
                                        cache_line,
                                        &result,
                                        debug);
              break;
        }

      }
      EXPECT_EQ(result.dirty_eviction, false);
      break;
  }
  
}

/*
 * TEST6: Verifies an error is return when invalid parameters are pass
 * performed.
 * 1. Choose a random policy 
 * 2. Choose invalid parameters for idx, tag and associativity
 * 3. Check function returns a PARAM error condition
 */
TEST_F(L1cache, boundaries){

  int status;
  int idx;
  int tag;
  int i;
  int associativity;
  bool loadstore = 1;
  bool debug = true;
  operation_result result = {};

  int policy = rand()%3;
  if(debug){
        switch(policy){
          case 0:
            printf("Testing for LRU:\n");
            break;
          case 1:
            printf("Testing for NRU:\n");
            break;
          case 2:
            printf("Testing for SRRIP:\n");
            break;
          default:
            printf("Testing for LRU:\n");
            break;
        }
  }

  idx = rand()%1024;
  tag = rand()%4096;
  associativity = 1 << (rand()%4);

  entry cache_line[associativity];

  /* Fill cache line */
    for ( i =  0; i < associativity; i++) {
      cache_line[i].valid = true;
      cache_line[i].tag = rand()%4096;
      cache_line[i].dirty = 0;

      switch(policy){
        case 0:
          cache_line[i].rp_value = i;
          break;
        case 1:
          cache_line[i].rp_value = 1;
          break;
        case 2:
          cache_line[i].rp_value = (associativity <= 2)? associativity-1: 3;
          break;
        default:
          cache_line[i].rp_value = i;
          break;
      }

      while (cache_line[i].tag == tag) {
        cache_line[i].tag = rand()%4096;
      }
    }

  //Checks invalid idx
  idx = -1;
  DEBUG(debug_on,Checking error with invalid index);
  if (debug) {
    printf("Checking invalid index: \nEntry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }
  switch(policy) {
        case 0:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 1:
            status = nru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 2:
            status = srrip_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        default:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
  }
  if (debug) {
    printf("Returned status: %d\n", status);
  }
  EXPECT_EQ(status, 1);

  //Checks invalid tag
  idx = rand()%1024;
  tag = -1;
  DEBUG(debug_on,Checking error with invalid tag);
  if (debug) {
    printf("Checking invalid tag: \nEntry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  switch(policy) {
        case 0:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 1:
            status = nru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 2:
            status = srrip_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        default:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
  }
  if (debug) {
    printf("Returned status: %d\n", status);
  }
  EXPECT_EQ(status, 1);

  //Checks invalid associativity
  tag = rand()%4096;
  associativity = -1;

  DEBUG(debug_on,Checking error with invalid associativity);
  if (debug) {
    printf("Checking invalid associativity: \nEntry Info\n idx: %d\n tag: %d\n associativity: %d\n",
          idx,
          tag,
          associativity);
  }

  switch(policy) {
        case 0:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 1:
            status = nru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        case 2:
            status = srrip_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
        default:
            status = lru_replacement_policy(idx, 
                                      tag, 
                                      associativity,
                                      loadstore,
                                      cache_line,
                                      &result,
                                      debug);
            break;
  }
  if (debug) {
    printf("Returned status: %d\n", status);
  }
  EXPECT_EQ(status, 1);

}
