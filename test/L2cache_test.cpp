/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include "debug_utilities.h"
#include "L1cache.h"
#include "L2cache.h"

#define STORE true
#define LOAD false

using namespace std;

class L2cache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST0: Verifica que se devuelve un error por parametros incorrectos
 * 1. Eliga valores incorrectos para idx, tag, offset y associativity
 * 3. Revisa que la funcion devuelva error PARAM
 */
 TEST_F(L2cache,parameters){
	DEBUG(debug_on, parameters);
	int status = OK;

	// Resultados
	operation_result l1_result = {};
	operation_result l2_result = {};

	// Se genera una configuración aleatoria de cache
	parameters params = random_params();
	params.opt = L2;
	// Se calculan los sizes
	sizes sizes = get_sizes(params);

	// Se genera un acceso aleatorio
	line_info access = random_access();

	//loadstore aleatorio
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, L2);
	}

	// Se genera un entry_info a partir de la direccion de acceso
	entry_info A = get_entry_info(access.address,sizes);

	// Elige un parametro aleatorio que invalidar
	int parametro_invalido = rand()%8;

	// Se crean L1 y L2
	entry l1_cache_blocks[sizes.l1_assoc] = {};
	entry l2_cache_blocks[sizes.l2_assoc] = {};

	switch(parametro_invalido){
		case(0):
			A.l1_tag = -1;
			break;
		case(1):
			A.l1_idx = -1;
			break;
		case(2):
			A.l1_offset = -1;
			break;
		case(3):
			A.l1_assoc = -1;
			break;
		case(4):
			A.l2_tag = -1;
			break;
		case(5):
			A.l2_idx = -1;
			break;
		case(6):
			A.l2_offset = -1;
			break;
		case(7):
			A.l2_assoc = -1;
			break;
	}

	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
	EXPECT_EQ(status,PARAM);

 }


/*
 * TEST1: Verifica que no se da dirty evictions con READ requests
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Llena el cache L2 (y el L1) con solo LOADS
 * 4. Se inserta un entry A aleatorio
 * 5. Se verifica que el evicted entry no fue dirty eviction
 */
TEST_F(L2cache,l2_read_dirty_eviction){
	DEBUG(debug_on, l2_read_dirty_eviction);
	int status = OK;
	operation_result l1_result = {};
	operation_result l2_result = {};

	// Se genera una configuración aleatoria de cache
	parameters params = random_params();
	params.opt = L2;
	// Se calculan los sizes
	sizes sizes = get_sizes(params);

	// Se genera un acceso aleatorio
	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, L2);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

	// Se genera un entry_info a partir de la direccion de acceso
	entry_info A = get_entry_info(access.address,sizes);

	// Se crean L1 y L2
	entry l1_cache_blocks[sizes.l1_assoc] = {};
	entry l2_cache_blocks[sizes.l2_assoc] = {};

	// Se llenan L1 y L2 con solo READ requests (LOADS)
	for (size_t i = 0; i < sizes.l2_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		//se deben forzar tags distintos, mismo indice (misma linea) y que no hayan repetidos
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_l2(&B,LOAD,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
		EXPECT_EQ(status,OK);
	}

	// Print state
	if(debug_on){
		printf("\n** Before insertion of block A **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}


	// Se inserta A y se obtiene un eviction
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status,OK);

	// Se prueba a ver si fue dirty eviction
	// L1 siempre debe ser false, puesto que usa write through
	EXPECT_EQ(l1_result.dirty_eviction,false);
	// L2 debe ser false puesto que el cache se lleno con read requests
	EXPECT_EQ(l2_result.dirty_eviction,false);

	// Print state
	if(debug_on){
		printf("\n** After insertion of block A**\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}
}

/*
 * TEST2: Verifica que se dan dirty evictions con WRITE requests
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Llena el cache L2 (y el L1) con solo STORES
 * 4. Se inserta un entry A aleatorio
 * 5. Se verifica que el evicted entry fue dirty eviction para L2
 */
TEST_F(L2cache,l2_write_dirty_eviction){
	DEBUG(debug_on, l2_write_dirty_eviction);
	int status = OK;
	operation_result l1_result = {};
	operation_result l2_result = {};

	// Se genera una configuración aleatoria de cache
	parameters params = random_params();
	params.opt = L2;
	// Se calculan los sizes
	sizes sizes = get_sizes(params);

	// Se genera un acceso aleatorio
	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, L2);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

	// Se genera un entry_info a partir de la direccion de acceso
	entry_info A = get_entry_info(access.address,sizes);

	// Se crean L1 y L2
	entry l1_cache_blocks[sizes.l1_assoc] = {};
	entry l2_cache_blocks[sizes.l2_assoc] = {};

	// Se llenan L1 y L2 con solo READ requests (LOADS)
	for (size_t i = 0; i < sizes.l2_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		//Se fuerzan misses para llenar los caches
		while(A.l1_tag == B.l1_tag || A.l2_tag == B.l2_tag || A.l1_idx != B.l1_idx || A.l2_idx != B.l2_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag) || is_in_set(l2_cache_blocks,B.l2_assoc,B.l2_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_l2(&B,STORE,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
		EXPECT_EQ(status,OK);
	}

	// Print state
	if(debug_on){
		printf("\n** Before insertion of block A **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}


	// Se inserta A y se obtiene un eviction
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status,OK);

	// Se prueba a ver si fue dirty eviction
	// L1 siempre debe ser false, puesto que usa write through
	EXPECT_EQ(l1_result.dirty_eviction,false);
	// L2 debe ser true puesto que el cache se lleno con write requests
	EXPECT_EQ(l2_result.dirty_eviction,true);

	// Print state
	if(debug_on){
		printf("\n** After insertion of block A**\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}
}


/*
 * TEST3:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 5. Se llena L1 al producir accesos distintos que A
 * 6. Se genera un miss en L1 y L2 al accesar A
 * 7. Se accesa A para producir un hit en L1 y L2
 * 8. Se verifica que hay hit en L1 y L2
 */
TEST_F(L2cache,l1_hit_l2_hit){
	DEBUG(debug_on, l1_hit_l2_hit);
	int status = OK;
	operation_result l1_result = {};
	operation_result l2_result = {};

	// Se genera una configuración aleatoria de cache
	parameters params = random_params();
	params.opt = L2;
	// Se calculan los sizes
	sizes sizes = get_sizes(params);

	// Se genera un acceso aleatorio
	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, L2);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

	// Se genera un entry_info a partir de la direccion de acceso
	entry_info A = get_entry_info(access.address,sizes);

	// Se crean L1 y L2
	entry l1_cache_blocks[sizes.l1_assoc] = {};
	entry l2_cache_blocks[sizes.l2_assoc] = {};

	// Se llenan L1 y L2
	for (size_t i = 0; i < sizes.l2_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		//Se fuerzan misses para llenar los caches
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag) || is_in_set(l2_cache_blocks,B.l2_assoc,B.l2_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_l2(&B,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
		EXPECT_EQ(status,OK);
	}

	// Print state
	if(debug_on){
		printf("\n** Before insertion of block A **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}


	// Se inserta A
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** After insertion of block A, before forced hit **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se referencia a A
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status, OK);

	// Print state
	if(debug_on){
		printf("\n** After forced hit **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	int expected = (loadstore) ? HIT_STORE : HIT_LOAD;
	// Comprobación de hit en L1
	EXPECT_EQ(l1_result.miss_hit,expected);
	// Comprobación de hit en L2
	EXPECT_EQ(l2_result.miss_hit,expected);

}



/*
 * TEST4:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 4. Se inserta A en el sistema de caches
 * 5. Se victimiza A de L1 al producir accesos distintos que A.
 * 6. Se genera un miss en L1 y hit en L2 al accesar A.
 */
TEST_F(L2cache,l1_miss_l2_hit){
	DEBUG(debug_on, l1_miss_l2_hit);
	int status = OK;
	operation_result l1_result = {};
	operation_result l2_result = {};

	// Se genera una configuración aleatoria de cache
	parameters params = random_params();
	params.opt = L2;

	// Se calculan los sizes
	sizes sizes = get_sizes(params);

	// Se genera un acceso aleatorio
	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, L2);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

	// Se genera un entry_info a partir de la direccion de acceso
	entry_info A = get_entry_info(access.address,sizes);

	// Se crean L1 y L2
	entry l1_cache_blocks[sizes.l1_assoc] = {};
	entry l2_cache_blocks[sizes.l2_assoc] = {};

	// Print state
	if(debug_on){
		printf("\n** Empty L1 and L2 **\n");
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se inserta A en L1 y L2
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** Block A has been inserted **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se hacen accsesos hasta victimizar A de L1 (pero A sigue estando en L2)
	for (size_t i = 0; i < sizes.l1_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_l2(&B,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
		EXPECT_EQ(status,OK);
	}

	// Print state
	if(debug_on){
		printf("\n** Block A has been victimized from L1 but not from L2 **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se referencia a A: esto fuerza miss en L1 y Hit en L2.
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status, OK);

	// Comprobación de miss en L1
	int expected_l1 = (loadstore) ? MISS_STORE : MISS_LOAD;
	EXPECT_EQ(l1_result.miss_hit,expected_l1);
	// Comprobación de hit en L2
	int expected_l2 = (loadstore) ? HIT_STORE : HIT_LOAD;
	EXPECT_EQ(l2_result.miss_hit,expected_l2);
}


/*
 * TEST5:
 * 1. Se genera una configuración aleatoria de cache
 * 2. Se calculan los sizes
 * 3. Se genera un acceso aleatorio A
 * 4. Se inserta A en el sistema de caches
 * 5. Se victimiza A de L1 y L2 al producir accesos distintos que A.
 * 6. Se genera un miss en L1 y en L2 al accesar A.
 */
TEST_F(L2cache,l1_miss_l2_miss){
	DEBUG(debug_on, l1_miss_l2_miss);
	int status = OK;
	operation_result l1_result = {};
	operation_result l2_result = {};

	// Se genera una configuración aleatoria de cache
	parameters params = random_params();
	params.opt = L2;

	// Se calculan los sizes
	sizes sizes = get_sizes(params);

	// Se genera un acceso aleatorio
	line_info access = random_access();
	bool loadstore = rand()%2;

	// Print test params:
	if(debug_on){
		printParams(params);
		print_sizes(sizes, L2);
		printf("** The original memory access for block A: 0x%X **\n", access.address);
	}

	// Se genera un entry_info a partir de la direccion de acceso
	entry_info A = get_entry_info(access.address,sizes);

	// Se crean L1 y L2
	entry l1_cache_blocks[sizes.l1_assoc] = {};
	entry l2_cache_blocks[sizes.l2_assoc] = {};

	// Print state
	if(debug_on){
		printf("\n** Empty L1 and L2 **\n");
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se inserta A en L1 y L2
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
	EXPECT_EQ(status,OK);

	// Print state
	if(debug_on){
		printf("\n** Block A has been inserted **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se hacen accsesos hasta victimizar A de L1 y de L2
	for (size_t i = 0; i < sizes.l2_assoc; i++){
		line_info  access = random_access();
		entry_info B = get_entry_info(access.address,sizes);
		while(A.l1_tag == B.l1_tag || A.l1_idx != B.l1_idx || is_in_set(l1_cache_blocks,B.l1_assoc,B.l1_tag) || is_in_set(l2_cache_blocks,B.l2_assoc,B.l2_tag)){
			access = random_access();
			B = get_entry_info(access.address,sizes);
		}
		status = lru_replacement_policy_l1_l2(&B,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,false);
		EXPECT_EQ(status,OK);
	}

	// Print state
	if(debug_on){
		printf("\n** Block A has been victimized from L1 and L2 **\n");
		printf("Tag of A in L1: 0x%X\n",A.l1_tag);
		printf("Tag of A in L2: 0x%X\n",A.l2_tag);
		print_set(l1_cache_blocks,sizes.l1_assoc,"L1 |");
		print_set(l2_cache_blocks, sizes.l2_assoc,"L2 |");
	}

	// Se referencia a A: esto fuerza miss en L1 y miss en L2.
	status = lru_replacement_policy_l1_l2(&A,loadstore,l1_cache_blocks,l2_cache_blocks,&l1_result,&l2_result,debug_on);
	EXPECT_EQ(status, OK);

	// Comprobación de miss en L1
	int expected_l1 = (loadstore) ? MISS_STORE : MISS_LOAD;
	EXPECT_EQ(l1_result.miss_hit,expected_l1);
	// Comprobación de hit en L2
	int expected_l2 = (loadstore) ? MISS_STORE : MISS_LOAD;
	EXPECT_EQ(l2_result.miss_hit,expected_l2);
}

