/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define KB 1024
#define ADDRSIZE 32
#define HIT true
#define MISS false
#define STORE true
#define LOAD false
#define LRU 0

using namespace std;

int l1_l2_entry_info_get (const struct cache_params l1_params,
                          const struct cache_params l2_params,
			  			  long address,
                          entry_info* l1_l2_info,
			  			  bool debug)
{
	int l1_size = l1_params.size;
	int l1_asociativity = l1_params.asociativity;
	int l1_block_size = l1_params.block_size;

	if(debug) {
		cout << "l1_l2_entry_info_get START" << endl;
		cout << "l1_size = " << l1_size << endl;
		cout << "l1_asociativity = " << l1_asociativity << endl;
		cout << "l1_block_size = " << l1_block_size << endl;
	}

	int l2_size = l2_params.size;
	int l2_asociativity = l2_params.asociativity;
	int l2_block_size = l2_params.block_size;

	if(debug) {
		cout << "l2_size = " << l2_size << endl;
		cout << "l2_asociativity = " << l2_asociativity << endl;
		cout << "l2_block_size = " << l2_block_size << endl;
	}

	// No se permiten valores negativos en L1
	if(l1_size <= 0 || l1_asociativity <= 0 || l1_block_size <= 0){
		return PARAM;
	}

	// Todos los tamaños deben ser potencias de 2 en L1
	if((ceil(log2(l1_size*KB)) != floor(log2(l1_size*KB))) ||
		(ceil(log2(l1_asociativity)) != floor(log2(l1_asociativity))) ||
		(ceil(log2(l1_block_size)) != floor(log2(l1_block_size)))){
			return PARAM;
	}

	// No se permiten valores negativos en L2
	if(l2_size <= 0 || l2_asociativity <= 0 || l2_block_size <= 0){
		return PARAM;
	}

	// Todos los tamaños deben ser potencias de 2 en L2
	if((ceil(log2(l2_size*KB)) != floor(log2(l2_size*KB))) ||
		(ceil(log2(l2_asociativity)) != floor(log2(l2_asociativity))) ||
		(ceil(log2(l2_block_size)) != floor(log2(l2_block_size)))){
			return PARAM;
	}

	// Field size
	int l1_offset_size = log2(l1_block_size);
	int l1_idx_size    = log2((l1_size*KB)/(l1_asociativity*l1_block_size));
	int l1_tag_size    = ADDRSIZE - (l1_idx_size  + l1_offset_size);

	int l2_offset_size = log2(l2_block_size);
	int l2_idx_size    = log2((l2_size*KB)/(l2_asociativity*l2_block_size));
	int l2_tag_size    = ADDRSIZE - (l2_idx_size  + l2_offset_size);

	if(debug) {
		cout << "l1_offset_size = " << l1_offset_size << endl;
		cout << "l1_idx_size = " << l1_idx_size << endl;
		cout << "l1_tag_size = " << l1_tag_size << endl;

		cout << "l2_offset_size = " << l2_offset_size << endl;
		cout << "l2_idx_size = " << l2_idx_size << endl;
		cout << "l2_tag_size = " << l2_tag_size << endl;
	}

	// Operaciones bitwise para extraer los bits respectivos 
	int l1_idx = (address >> l1_offset_size ) & ((1 << l1_idx_size ) - 1);
	int l1_tag =  address >> (l1_idx_size  + l1_offset_size );

	int l2_idx = (address >> l2_offset_size ) & ((1 << l2_idx_size ) - 1);
	int l2_tag =  address >> (l2_idx_size  + l2_offset_size );

	if(debug) {
		cout << "Address = " << address << endl;
		cout << "l1_idx= " << l1_idx << endl;
		cout << "l1_tag = " << l1_tag << endl;

		cout << "l2_idx= " << l2_idx << endl;
		cout << "l2_tag = " << l2_tag << endl;
	}

	// Se llenan el struct
	l1_l2_info->l1_tag = l1_tag;
	l1_l2_info->l1_idx = l1_idx;
	l1_l2_info->l1_assoc = l1_asociativity;

	l1_l2_info->l2_tag = l1_tag;
	l1_l2_info->l2_idx = l2_idx;
	l1_l2_info->l2_assoc = l2_asociativity;

	return OK;
}

int lru_replacement_policy_l1_l2(const entry_info *l1_l2_info,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 entry* l2_cache_blocks,
				 operation_result* l1_result,
				 operation_result* l2_result,
				 bool debug)
{

	// No se permiten valores negativos
	if ((int) l1_l2_info->l1_tag < 0 || (int) l1_l2_info->l1_idx < 0 || (int) l1_l2_info->l1_assoc <= 0 || (int) l1_l2_info->l1_offset < 0) {
		return PARAM;
	}

	if ((int) l1_l2_info->l2_tag < 0 || (int) l1_l2_info->l2_idx < 0 || (int) l1_l2_info->l2_assoc <= 0 || (int) l1_l2_info->l2_offset < 0) {
		return PARAM;
	}


	// Valores del L1
	u_int l1_tag = l1_l2_info->l1_tag;
   	u_int l1_idx = l1_l2_info->l1_idx;
   	u_int l1_offset = l1_l2_info->l1_offset;
	u_int l1_assoc = l1_l2_info->l1_assoc;
	int l1_MRU = l1_assoc - 1;

	// Valores del L2
	u_int l2_tag = l1_l2_info->l2_tag;
   	u_int l2_idx = l1_l2_info->l2_idx;
   	u_int l2_offset = l1_l2_info->l2_offset;
	u_int l2_assoc = l1_l2_info->l2_assoc;
	int l2_MRU = l2_assoc - 1;

	// // Asociatividad tiene que ser potencia de 2
	// if (ceil(log2(l1_assoc)) != floor(log2(l1_assoc)) || ceil(log2(l2_assoc)) != floor(log2(l2_assoc))) {
	// 	return PARAM;
	// }

	// Valores iniciales
  	bool l1_miss_hit = MISS;
	bool l2_miss_hit = MISS;
	l1_result->dirty_eviction = false;
	l2_result->dirty_eviction = false;


	/*******************************
			HAY HIT EN L1
	********************************/

	// Revisamos las vías de L1
	for (int l1_way = 0; l1_way < l1_assoc; l1_way++) {
		
		/***************************************
					HIT EN L1 Y L2
		Como es inclusivo, si se da un hit en L1
		el mismo bloque se encuentra en L2.
		****************************************/
		if ((l1_cache_blocks[l1_way].tag == l1_tag) && l1_cache_blocks[l1_way].valid) {

			// HIT en L1
			l1_miss_hit = HIT;
			
			// Actualiza los valores de RP para L1
			for (int l1_way_rp = 0; l1_way_rp < l1_assoc; l1_way_rp++) {
				if (l1_cache_blocks[l1_way_rp].rp_value > l1_cache_blocks[l1_way].rp_value) {
					l1_cache_blocks[l1_way_rp].rp_value--;
				}
			}
			
			// Pasa el bloque al MRU
			l1_cache_blocks[l1_way].rp_value = l1_MRU;

			// Buscamos por las vías de L1 por el mismo tag
			// No hay posibilidad de que haya un miss por la 
			// política inclusiva
			for (int l2_way = 0; l2_way < l2_assoc; l2_way++) {
				if (l2_cache_blocks[l2_way].tag == l2_tag) {
					
					// HIT en L2
					l2_miss_hit = HIT;

					// Actualiza los valores de RP para L2
					for (int l2_way_rp = 0; l2_way_rp < l2_assoc; l2_way_rp++) {
						if (l2_cache_blocks[l2_way_rp].rp_value > l2_cache_blocks[l2_way].rp_value) {
							l2_cache_blocks[l2_way_rp].rp_value--;
						}
					}

					// Pasa el bloque al MRU
					l2_cache_blocks[l2_way].rp_value = l2_MRU;

					// Resultados
					if (loadstore == STORE) {
						// Entre L2 y memoria hay política write-back
						// por lo que solo aplica la asignacion de
						// dirty a este cache
						l2_cache_blocks[l2_way].dirty = true;
						
						l2_result->miss_hit = HIT_STORE;
						l1_result->miss_hit = HIT_STORE;
					}
					else {
						l2_result->miss_hit = HIT_LOAD;
						l1_result->miss_hit = HIT_LOAD;
					}
					
					return OK;
				}
			}

			// No debería pasar que no encuentra el
			// bloque en L2
			return ERROR;
		}
	}
		
	
	/*******************************
		 	HAY MISS EN L1
	********************************/

	// Buscamos por un HIT en L2
	if (l1_miss_hit == MISS) {
		for (int l2_way = 0; l2_way < l2_assoc; l2_way++) {

			/***************************************
					MISS EN L1 Y HIT EN L2
				   Se debe reemplazar en L1.
			****************************************/

			if (l2_cache_blocks[l2_way].tag == l2_tag && l2_cache_blocks[l2_way].valid) {

				// HIT en L2
				l2_miss_hit = HIT;

				// Actualiza los valores de RP para L2
				for (int l2_way_rp = 0; l2_way_rp < l2_assoc; l2_way_rp++) {
					if (l2_cache_blocks[l2_way_rp].rp_value > l2_cache_blocks[l2_way].rp_value) {
							l2_cache_blocks[l2_way_rp].rp_value--;
					}
				}

				// Pasa el bloque al MRU
				l2_cache_blocks[l2_way].rp_value = l2_MRU;

				// Resultados
				if (loadstore == STORE) {
					l2_cache_blocks[l2_way].dirty = true;
					l2_result->miss_hit = HIT_STORE;
				} else {
					l2_result->miss_hit = HIT_LOAD;
				}

				// Se reemplaza en L1
				replace_in_L1(l1_assoc, l1_tag, loadstore, l1_cache_blocks, l1_result);

				return OK;
			}
		}
	}	
	
	
	/***************************************
			MISS EN L1 Y MISS EN L2
		Se debe reemplazar en L1 y L2
	****************************************/
	
	if (l2_miss_hit == MISS) {
		// Primero se reemplaza en L2
		for (int l2_way = 0; l2_way < l2_assoc; l2_way++) {
			if (l2_cache_blocks[l2_way].rp_value == LRU){ // Se da un eviction
				
				// Dirty?
				l2_result->dirty_eviction = l2_cache_blocks[l2_way].dirty;
				
				// Evicted address
				l2_result->evicted_address = l2_cache_blocks[l2_way].tag;

				//Resultados
				if (loadstore == STORE) {
					l2_cache_blocks[l2_way].dirty = true;
					l2_result->miss_hit = MISS_STORE;
				} else {
					l2_cache_blocks[l2_way].dirty = false;
					l2_result->miss_hit = MISS_LOAD;
				}

				// Actualiza RPV
				for (int l2_way_rp = 0; l2_way_rp < l2_assoc; l2_way_rp++) {
					if (l2_cache_blocks[l2_way_rp].rp_value != LRU){
			 			l2_cache_blocks[l2_way_rp].rp_value--;
					 }
				}

				// Actualiza el nuevo entry ingresado
				l2_cache_blocks[l2_way].rp_value = l2_MRU;
				l2_cache_blocks[l2_way].valid = true;
				l2_cache_blocks[l2_way].tag = l2_tag;
				
				// Se reemplaza en L1
				replace_in_L1(l1_assoc, l1_tag, loadstore, l1_cache_blocks, l1_result);

				return OK;
			}
		}
	}

	return ERROR;
}


void replace_in_L1(u_int l1_assoc,
				 u_int l1_tag,
				 bool loadstore,
				 entry* l1_cache_blocks,
				 operation_result* l1_result)
{
	for (int l1_way = 0; l1_way < l1_assoc; l1_way++) {
		if (l1_cache_blocks[l1_way].rp_value == LRU){
			
			l1_result->dirty_eviction = false;
			
			// Evicted address
			l1_result->evicted_address = l1_cache_blocks[l1_way].tag;

			// Resultados
			if (loadstore == STORE) {
				l1_result->miss_hit = MISS_STORE;
			} else {
				l1_result->miss_hit = MISS_LOAD;
			}

			// Actualiza RPV
			for (int l1_way_rp = 0; l1_way_rp < l1_assoc; l1_way_rp++) {
				if (l1_cache_blocks[l1_way_rp].rp_value != LRU){
		 			l1_cache_blocks[l1_way_rp].rp_value--;
				 }
			}

			// Actualiza entry
			l1_cache_blocks[l1_way].rp_value = l1_assoc - 1; // MRU
			l1_cache_blocks[l1_way].valid = true;
			l1_cache_blocks[l1_way].tag = l1_tag;

			break; //Ya se encontro el LRU
		}
	}
}

