#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <L1cache.h>
#include <debug_utilities.h>

#define KB 1024
#define miss_penalty 20
using namespace std;

/* Helper functions */

/* Print cache configuration */
void print_cache_config(cache_params params) {
  string divisory_line = "___________________________________________";

  cout << divisory_line << endl;
  cout << "Cache parameters:" << endl;
  cout << divisory_line << endl;

  cout << "Cache Size (KB): \t\t" << params.size << endl;;
  cout << "Cache Associativity: \t\t" << params.asociativity << endl;;
  cout << "Cache Block Size (bytes): \t" << params.block_size << endl;

}

void print_statistics(statistics stats) {
  
  float l1_misses = stats.load_misses + stats.store_misses;
  float l1_hits = stats.load_hits + stats.store_hits;

  float read_miss_rate = (float) (stats.load_misses) / (float) (stats.load_misses + stats.load_hits);
  int cpu_time = stats.instruction_count + l1_misses * miss_penalty;
  float amat = (l1_hits + l1_misses*(1 + miss_penalty))/(l1_misses + l1_hits);

  string divisory_line = "___________________________________________";

  cout << divisory_line << endl;
  cout << "Simulation results:" << endl;
  cout << divisory_line << endl;

  cout << "CPU time (cycles): \t\t" << cpu_time << endl;
  cout << "AMAT (cycles): \t\t\t" << amat << endl;
  cout << "Overall miss rate: \t\t" << l1_misses/ (l1_misses + l1_hits) << endl;
  cout << "Read miss rate: \t\t" << read_miss_rate << endl;
  cout << "Dirty evictions: \t\t" << stats.dirty_evictions << endl;
  cout << "Load misses: \t\t\t" << stats.load_misses << endl;
  cout << "Stores misses: \t\t\t"  << stats.store_misses << endl;
  cout << "Total misses: \t\t\t"  << stats.load_misses + stats.store_misses << endl;
  cout << "Load hits: \t\t\t" << stats.load_hits << endl;
  cout << "Stores hits: \t\t\t"  << stats.store_hits << endl;
  cout << "Total hits: \t\t\t"  << stats.load_hits + stats.store_hits << endl;
  cout << divisory_line << endl;
 
}



int main(int argc, char * argv []) {

  /* Parse argruments */

  uint16_t l1_size, l1_block_size, l1_asociativity;
  replacement_policy l1_policy;

  for (size_t i = 0; i < argc; i++) {
    
    // Revisión simple de suficientes argumentos
    if (argc%2 == 0) {
      cout << "Faltan argumentos" << endl;
      exit (EXIT_FAILURE);
    }
    // Tamaño del cache en KB
    if (strcmp(argv[i], "-t") == 0) {
      l1_size = atoi(argv[i+1]);
    }
    // Tamaño de la línea en bytes
    else if (strcmp(argv[i], "-l") == 0) {
      l1_block_size = atoi(argv[i+1]);
    }
    // Asociatividad
    else if (strcmp(argv[i], "-a") == 0) {
      l1_asociativity = atoi(argv[i+1]);
    }
    // Política de reemplazo
    else if (strcmp(argv[i], "-rp") == 0) {
    	if (strcmp(argv[i+1], "lru") == 0) {
    		l1_policy = LRU;
    	}
    	else if (strcmp(argv[i+1], "nru") == 0) {
    		l1_policy = NRU;
    	}
      else if (strcmp(argv[i+1], "rrip") == 0) {
    		l1_policy = RRIP;
    	}
      else if (strcmp(argv[i+1], "srrip") == 0) {
    		l1_policy = RRIP;
    	}
    	else {
    		cout << "Política no encontrada" << endl;
        cout << "Opciones válidas son:\nlru, nru y rrip." << endl;
        exit (EXIT_FAILURE);
    	}
    }
  }

  /* Build cache */

  int num_sets = (l1_size*KB)/(l1_block_size*l1_asociativity);
  struct entry l1_cache[num_sets][l1_asociativity];

  // Valores iniciales
  for(int i = 0; i < num_sets; i++) {
    for (int j = 0; j < l1_asociativity; j++) {
      l1_cache[i][j].valid = false;
      l1_cache[i][j].dirty = false;

      // Valores iniciales de RPV para cada politica
      switch(l1_policy) {
        case NRU:
          l1_cache[i][j].rp_value = 1;
          break;
        case RRIP:
          l1_cache[i][j].rp_value = 3;
          break;
        default:
          l1_cache[i][j].rp_value = 0;
          break;
      }

      l1_cache[i][j].tag = 0;
      
      l1_cache[i][j].obl_tag = false;
    }
  }


  // Obtención de tamaño de tag, idx y offset

  cache_params * l1_params = new cache_params();
  l1_params->size = l1_size;
  l1_params->asociativity = l1_asociativity;
  l1_params->block_size = l1_block_size;

  cache_field_size * l1_field = new cache_field_size();
  field_size_get(*l1_params, l1_field);

  

  /* Get trace's lines and start your simulation */
  struct statistics l1_stats = {};
  string trace;
  while (getline(cin,trace)) {
    
    // Trace parsing
    bool loadstore = stoi(trace.substr(2, 2), nullptr, 2);
    long address = stoi(trace.substr(4, 8), nullptr, 16);
    int  instruction_count = stoi(trace.substr(13, 13));

    // Get idx y tag
    int l1_idx, l1_tag;
    
    address_tag_idx_get(address, *l1_field, &l1_idx, &l1_tag);

    // Se recorre el trace realizando la política de reemplazo respectiva
    struct operation_result l1_result = {};
    int status;
    
    switch(l1_policy) {
      case LRU :
          status = lru_replacement_policy(l1_idx, 
                                      l1_tag, 
                                      l1_params->asociativity,
                                      loadstore,
                                      l1_cache[l1_idx],
                                      &l1_result,
                                      false);
          break;
      case NRU  :
          status = nru_replacement_policy(l1_idx, 
                            l1_tag, 
                            l1_params->asociativity,
                            loadstore,
                            l1_cache[l1_idx],
                            &l1_result,
                            false);
          break;
      case RRIP  :
          status = srrip_replacement_policy(l1_idx, 
                            l1_tag, 
                            l1_params->asociativity,
                            loadstore,
                            l1_cache[l1_idx],
                            &l1_result,
                            false);
          break;
    }

    get_statistics(l1_result, &l1_stats, instruction_count);   
  }

  /* Print cache configuration */
  print_cache_config(*l1_params);

  /* Print Statistics */
  print_statistics(l1_stats);
  
  return 0;
}
