/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
using namespace std;

int lru_replacement_policy_l1_vc(const entry_info  *l1_vc_info,
    	                     bool loadstore,
       	                   entry* l1_cache_blocks,
       	                   entry* vc_cache_blocks,
        	                 operation_result* l1_result,
              	           operation_result* vc_result,
                	         bool debug)
{

   int tag= (int)l1_vc_info-> l1_tag;
   int idx= (int)l1_vc_info-> l1_idx;
   int associativity=(int)l1_vc_info-> l1_assoc;
   int vc_associativity=(int)l1_vc_info-> vc_assoc;
   bool dato_presente = 0;
   int pos_libre = 0;
   bool libre = 0;
   int lru_max = 0;
   int pos_lru = 0;
   int pos_dato = 0;
   //Para el VC
   bool freeSpaceVC=false;
   int posLibreVC;
   bool hitVC=false;
   int posHitVC;
   bool primerUnoVC=false;
   int posunoVC;

   if(idx < 0 || associativity <= 0 || vc_associativity <= 0 || tag < 0 || (ceil(log2(associativity)) != floor(log2(associativity))) || (ceil(log2(vc_associativity)) != floor(log2(vc_associativity))) ){
      return PARAM;
   }

   if(debug){
      cout << "\nIniciando LRU en L1: "<< endl;
   }

   for(int i = 0; i < associativity; i++){
      if(l1_cache_blocks[i].valid == false){
         pos_libre = i; //guarda la ultima posicion libre que encuentra
         libre = 1;

      } else if(l1_cache_blocks[i].tag == tag){ //Hay hit en l1
         dato_presente=true;
         pos_dato = i; //Guarda la posicion del hit
         if(debug){
            cout << "Se encontró el dato válido en la posición: " << pos_dato << endl;
         }
          if(loadstore){ //hit store en l1 resulta en un miss store
             l1_result->miss_hit = HIT_STORE;
             vc_result->miss_hit = MISS_STORE;
             l1_cache_blocks[pos_dato].dirty = true;
             if(debug){
                cout << "Result miss_hit: " << l1_result->miss_hit << endl;
                print_way_info(idx,associativity,l1_cache_blocks);
             }
             return OK;
          } else { //hit load en l1 resulta en un miss load
             l1_result->miss_hit = HIT_LOAD;
             vc_result->miss_hit = MISS_LOAD;
             if(debug){
                cout << "Result miss_hit: " << l1_result->miss_hit << endl;
                print_way_info(idx,associativity,l1_cache_blocks);
             }
             return OK;
          }

         for(int a = 0; a < associativity; a++){ //Actualizamos el LRU tomando en cuenta el hit
            if(l1_cache_blocks[a].rp_value < l1_cache_blocks[i].rp_value){
               l1_cache_blocks[a].rp_value=l1_cache_blocks[a].rp_value+1;
            }
         }
         l1_cache_blocks[i].rp_value = 0;

      } else { //Actualizamos el LRU si no hay hit
         if(l1_cache_blocks[i].rp_value > lru_max){
            lru_max = l1_cache_blocks[i].rp_value;
            pos_lru = i;
         }
      }
   }

     if(!dato_presente){ //Caso sin hit en L1

       if (loadstore) {
         l1_result->miss_hit = MISS_STORE;
       }
       else{
         l1_result->miss_hit = MISS_LOAD;
       }



       if(debug){
          cout << "Revisamos en VC "  << endl;
       }
       for (int i = 0; i < vc_associativity; i++) {
         if (vc_cache_blocks[i].valid==false) { //Libre
           if (freeSpaceVC==false) { //Primera posicion libre de VC la guarda
             freeSpaceVC=true;
             posLibreVC=i;

           }
         } //Si es espacio libre
         else{
           if (vc_cache_blocks[i].tag==tag) { //Hay un hit
             posHitVC=i; //Se guarda la posicion del hit
             hitVC=true;
             if(debug){
                cout << "Se encontró el dato válido en VC en la posición: " << posHitVC << endl;
             }
           }
           else { //No hay hit
             if (vc_cache_blocks[i].rp_value==1 && !primerUnoVC) { //Si encuentra el primer uno, guarda la posicion
               primerUnoVC=true;
               posunoVC=i;
             }
           }
         }
       }


       if(libre && !hitVC){ //Caso: No hit en L1, posicion libre en L1 y no hit en VC
         if (loadstore) {
           vc_result->miss_hit = MISS_STORE;
           l1_cache_blocks[pos_libre].dirty = true;
         }
         else{
           vc_result->miss_hit = MISS_LOAD;
         }

         if(debug){
            cout << "Posición libre de L1: " << pos_libre << endl;
         }

         l1_cache_blocks[pos_libre].tag = tag;
         l1_cache_blocks[pos_libre].valid = true;

         for(int a = 0; a < associativity; a++){
            if(l1_cache_blocks[a].valid){
               l1_cache_blocks[a].rp_value=l1_cache_blocks[a].rp_value+1;
            }
         }
         l1_cache_blocks[pos_libre].rp_value = 0;

    }  if(!libre && !hitVC){ //Caso: No hit en L1, no hay espacio libre en L1 y no hay hit en VC
          if (loadstore) {
            vc_result->miss_hit = MISS_STORE;
          }
          else {
            vc_result->miss_hit = MISS_LOAD;
          }

        //Parte en L1
         if(debug){
            cout << "Posición del dato reemplazado: " << pos_lru << endl;
            cout << "Tag del dato reemplazado: " << l1_cache_blocks[pos_lru].tag << endl;
            cout << "Bit de dirty: " << l1_cache_blocks[pos_lru].dirty << endl;
         }
         l1_result->dirty_eviction = l1_cache_blocks[pos_lru].dirty;

         l1_result->evicted_address = l1_cache_blocks[pos_lru].tag;

         l1_cache_blocks[pos_lru].tag = tag; //Se sustituye por el nuevo tag ingresado

         if (loadstore) {
           l1_result->miss_hit = MISS_STORE;
           l1_cache_blocks[pos_lru].dirty = true;
         }
         else {
           l1_result->miss_hit = MISS_LOAD;
           l1_cache_blocks[pos_lru].dirty = false;
         }

         for(int a = 0; a < associativity; a++){ //Actualizamos el LRU
            l1_cache_blocks[a].rp_value=l1_cache_blocks[a].rp_value+1;

         }
         l1_cache_blocks[pos_lru].rp_value = 0;



         //Parte VC
         if (freeSpaceVC) { //Subcaso: Si hay espacio libre
           if(debug){
              cout << "Posición libre de VC: " << posLibreVC << endl;
           }
           vc_cache_blocks[posLibreVC].tag = l1_result->evicted_address; //Se guarda en la posicion libre el tag sacado de L1
           vc_cache_blocks[posLibreVC].valid = true;
           vc_cache_blocks[posLibreVC].rp_value = 0;
           vc_cache_blocks[posLibreVC].dirty = l1_result->dirty_eviction; //Se guarda el respectivo bit de dirty
         }
         else{ // si no hay espacio libre
           if (!primerUnoVC) { //En caso de que todos sean cero
             for (int i = 0; i < vc_associativity; i++) { //llena todas las vias de 1
               vc_cache_blocks[i].rp_value=1;
             }
             posunoVC=0; //La primera posicion para sustituir se
           }

           if(debug){
              cout << "Posición del dato reemplazado en VC: " << posunoVC << endl;
              cout << "Tag del dato reemplazado en VC: " << vc_cache_blocks[posunoVC].tag << endl;
              cout << "Bit de dirty: " << vc_cache_blocks[posunoVC].dirty << endl;
           }
           vc_result->dirty_eviction = vc_cache_blocks[posunoVC].dirty;//Se guarda los resultados de los evictions

           vc_result->evicted_address = vc_cache_blocks[posunoVC].tag;

           vc_cache_blocks[posunoVC].tag = l1_result->evicted_address;
           vc_cache_blocks[posunoVC].rp_value=0;
           vc_cache_blocks[posunoVC].dirty = l1_result->dirty_eviction;
         }
       }

       if (hitVC) { //Caso: No hay hit en L1 pero si en VC
         if(debug){
            cout << "Posición del dato reemplazado en L1: " << pos_lru << endl;
            cout << "Tag del dato reemplazado en L1: " << l1_cache_blocks[pos_lru].tag << endl;
            cout << "Bit de dirty: " << l1_cache_blocks[pos_lru].dirty << endl;
         }
         l1_result->dirty_eviction = l1_cache_blocks[pos_lru].dirty;
         //Se agregan los valores de eviction
         l1_result->evicted_address = l1_cache_blocks[pos_lru].tag;
         if (loadstore) {
           l1_result->miss_hit = MISS_STORE;
           l1_cache_blocks[pos_lru].dirty = true; //Se actualiza el bit de dirty segun el loadstore
         }
         else {
           l1_result->miss_hit = MISS_LOAD;
           l1_cache_blocks[pos_lru].dirty = false;
         }

         //Se recupera del VC
         l1_cache_blocks[pos_lru].tag = vc_cache_blocks[posHitVC].tag;

         for(int a = 0; a < associativity; a++){ //Se actualiza el LRU
            l1_cache_blocks[a].rp_value=l1_cache_blocks[a].rp_value+1;

         }
         l1_cache_blocks[pos_lru].rp_value = 0;

//////////////////////////////////////////////////////////////////////////////////

         if(debug){
            cout << "Posición del dato recuperado en VC: " << posHitVC << endl;
            cout << "Tag del dato recuperado en VC: " << vc_cache_blocks[posHitVC].tag << endl;
            cout << "Bit de dirty: " << vc_cache_blocks[posHitVC].dirty << endl;
         }



         vc_cache_blocks[posHitVC].tag = l1_result->evicted_address;
         vc_cache_blocks[posHitVC].rp_value=0;
         vc_cache_blocks[posHitVC].dirty=l1_result->dirty_eviction; //Se trae el bit de dirty del dato en l1
         if (loadstore) {
           vc_result->miss_hit = HIT_STORE;
         }
         else {
           vc_result->miss_hit = HIT_LOAD;
         }


       }


       if(debug){
          print_way_info(idx,associativity,l1_cache_blocks);
          print_way_info(idx,vc_associativity,vc_cache_blocks);
       }
       return OK;
    }










   return ERROR;
}
