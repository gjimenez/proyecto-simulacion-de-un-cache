/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>

#define KB 1024
#define ADDRSIZE 32
#define LOAD false
#define STORE true
using namespace std;

int field_size_get(struct cache_params cache_params,
                   struct cache_field_size *field_size)
{
   int size = cache_params.size;
   int asociativity = cache_params.asociativity;
   int block_size = cache_params.block_size;

   // No se permiten valores negativos
   if(size <= 0 || asociativity <= 0 || block_size <= 0){
      return PARAM;
   }

   // Todos los tamaños deben ser potencias de 2
   if((ceil(log2(size*KB)) != floor(log2(size*KB))) ||
      (ceil(log2(asociativity)) != floor(log2(asociativity))) ||
      (ceil(log2(block_size)) != floor(log2(block_size)))){
         return PARAM;
   }


   field_size->offset = log2(block_size);
   field_size->idx    = log2((size*KB)/(asociativity*block_size));
   field_size->tag   = ADDRSIZE - (field_size->idx  + field_size->offset);

   return OK;
}

void address_tag_idx_get(long address,
                         struct cache_field_size field_size,
                         int *idx,
                         int *tag)
{
   int idx_size = field_size.idx;
   int offset_size = field_size.offset;

   // Operaciones bitwise para extraer los bits respectivos
   *idx = (address >> offset_size ) & ((1 << idx_size ) - 1);
   *tag =  address >> (idx_size  + offset_size );

}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   if(idx < 0 || associativity <= 0 || tag < 0 || (ceil(log2(associativity)) != floor(log2(associativity)))){
      return PARAM;
   }

   int M = (associativity <= 2) ? 1:2;
   int saturation = pow(2, M) - 1;
   int long_interval = pow(2, M) - 2;
   int free_way = -1;
   int saturation_way = -1;

   if(debug){
      cout << "\nIniciando SRRIP: "<< endl;
      print_way_info(idx,associativity,cache_blocks);
   }
   // Revisamos las vías
   for(int i = 0; i < associativity; i++){
      if(cache_blocks[i].valid == true) {
         if(cache_blocks[i].tag == tag) { // Se da un hit

            result->miss_hit = (loadstore == LOAD) ? HIT_LOAD:HIT_STORE;

            // Se actualiza el entry
            cache_blocks[i].tag = tag;
            // El bit de dirty cambia a true si se da un STORE
            if(loadstore == STORE) {
               cache_blocks[i].dirty = true;
            }

            cache_blocks[i].rp_value = 0; //RRPV = 0

            if(debug){
               cout << "HIT!" << endl;
               cout << "Se dio un: ";
               if(result->miss_hit == HIT_LOAD){
                  cout << "Load Hit" << endl;
               }
               else {
                  cout << "Store Hit" << endl;
               }

               print_way_info(idx,associativity,cache_blocks);
            }

            return OK;
         }
         else { // Sigue buscando por las vias
            // Encuentra el primer bloque con RRPV con saturacion
            if (cache_blocks[i].rp_value == saturation && saturation_way == -1){
               saturation_way = i;
            }
         }

      }
      else {
         // Encuentra la primera via libre
         if(free_way == -1)
            free_way = i;

      }
   }

   // Se probaron todas las vías, hay un miss
   result->miss_hit = (loadstore == LOAD)? MISS_LOAD:MISS_STORE;

   if(free_way != -1) { // Hay una via libre
      // Se actualiza el entry
      cache_blocks[free_way].tag = tag;
      // El bit de dirty cambia a true si se da un STORE
      if(loadstore == STORE) {
         cache_blocks[free_way].dirty = true;
      }
      // El bit de dirty cambia a false si se da un LOAD MISS
      else {
         cache_blocks[free_way].dirty = false;
      }
      cache_blocks[free_way].valid = true;
      cache_blocks[free_way].rp_value = long_interval; //RRPV = 2^M - 2

      if(debug){
         cout << "miss..." << endl;
         cout << "Se dio un: ";
         if(result->miss_hit == MISS_LOAD){
            cout << "Load Miss" << endl;
         }
         else {
            cout << "Store Miss" << endl;
         }

         print_way_info(idx,associativity,cache_blocks);
      }

      return OK;
   }
   else { // No se encontró vía libre
      if (saturation_way == -1) { // No se encontró bloque con saturacion
         bool seEncontroSaturacion = false;
         // Se debe sumar uno a todos los bloques del set hasta tener uno de saturacion
         while (!seEncontroSaturacion){
            for(int i = 0; i < associativity; i++){
               cache_blocks[i].rp_value ++;
               // Encuentra el primer bloque con RRPV de saturacion
               if (cache_blocks[i].rp_value == saturation && saturation_way == -1){
                  saturation_way = i;
               }
            }

            // Si ya se recorrieron las vías y no se encontró un bloque con set de saturacion
            // se repite el proceso
            if(saturation_way != -1){
               seEncontroSaturacion = true;
            }
         }
      }

      // Se reemplaza la dirección según la política RRIP
      result->evicted_address = cache_blocks[saturation_way].tag;
      result->dirty_eviction  = cache_blocks[saturation_way].dirty;

      // Se actualiza el entry
      cache_blocks[saturation_way].tag = tag;
      // El bit de dirty cambia a true si se da un STORE
      if(loadstore == STORE) {
         cache_blocks[saturation_way].dirty = true;
      }
      // El bit de dirty cambia a false si se da un LOAD MISS
      else {
         cache_blocks[saturation_way].dirty = false;
      }
      cache_blocks[saturation_way].valid = true;
      cache_blocks[saturation_way].rp_value = long_interval; //RRPV = 2^M - 2

      if(debug){
         cout << "miss..." << endl;
         cout << "Se dio un: ";
         if(result->miss_hit == MISS_LOAD){
            cout << "Load Miss" << endl;
         }
         else {
            cout << "Store Miss" << endl;
         }

         cout << "Se reemplazó: " << result->evicted_address << endl;
         cout << "Fue dirty eviction?: " << result->dirty_eviction << endl;

         print_way_info(idx,associativity,cache_blocks);
      }

      return OK;
   }

   return ERROR;
}


int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{

   bool dato_presente = 0;
   int pos_libre = 0;
   bool libre = 0;
   int lru_max = 0;
   int pos_lru = 0;
   int pos_dato = 0;
   //Verificación de que los parámetros sean correctos
   if(idx < 0 || associativity <= 0 || tag < 0 || (ceil(log2(associativity)) != floor(log2(associativity)))){
      return PARAM;
   }

   if(debug){
      cout << "\nIniciando LRU: "<< endl;
   }
   //Se recorre cada entrada
   for(int i = 0; i < associativity; i++){
     //Se verifica si hay espacio vacío
      if(cache_blocks[i].valid == false){
         pos_libre = i;
         libre = 1;
      //Se verifica si hay un hit
      } else if(cache_blocks[i].tag == tag){
         dato_presente=true;
         pos_dato = i;
         if(debug){
            cout << "Se encontró el dato válido en la posición: " << pos_dato << endl;
         }
            if(loadstore){
               result->miss_hit = HIT_STORE;
               if(debug){
                  cout << "Result miss_hit: " << result->miss_hit << endl;
               }
            } else {
               result->miss_hit = HIT_LOAD;
               if(debug){
                  cout << "Result miss_hit: " << result->miss_hit << endl;
               }
            }
         //Se actualiza el valor de rp_value
         for(int a = 0; a < associativity; a++){
            if(cache_blocks[a].rp_value < cache_blocks[i].rp_value){
               cache_blocks[a].rp_value=cache_blocks[a].rp_value+1;
            }
         }
         cache_blocks[i].rp_value = 0;
      //Se verifica el dato a ser reemplazado
      } else {
         if(cache_blocks[i].rp_value > lru_max){
            lru_max = cache_blocks[i].rp_value;
            pos_lru = i;
         }
      }
   }

   if(loadstore == STORE){

      if(debug){
         cout << "Se realiza un store " << endl;
      }
      //Si hay un hit se actualiza el dirty
      if(dato_presente){
         cache_blocks[pos_dato].dirty = true;
      //Si hay una posición libre se pone el dato en la posición correspondiente, y se actualiza el rp_value
      } else if(libre){
         result->miss_hit = MISS_STORE;
         if(debug){
            cout << "Posición libre: " << pos_libre << endl;
         }
         cache_blocks[pos_libre].dirty = true;
         cache_blocks[pos_libre].tag = tag;
         cache_blocks[pos_libre].valid = true;

         for(int a = 0; a < associativity; a++){
            if(cache_blocks[a].valid){
               cache_blocks[a].rp_value=cache_blocks[a].rp_value+1;
            }
         }
         cache_blocks[pos_libre].rp_value = 0;
      //Si no se cumple alguna de las anteriores, se reemplaza el dato con el valor mayor de LRU y se actualiza el rp_value
      } else{
         result->miss_hit = MISS_STORE;
         if(debug){
            cout << "Posición del dato reemplazado: " << pos_lru << endl;
            cout << "Tag del dato reemplazado: " << cache_blocks[pos_lru].tag << endl;
            cout << "Bit de dirty: " << cache_blocks[pos_lru].dirty << endl;
         }
         result->dirty_eviction = cache_blocks[pos_lru].dirty;

         result->evicted_address = cache_blocks[pos_lru].tag;

         cache_blocks[pos_lru].dirty = true;
         cache_blocks[pos_lru].tag = tag;

         for(int a = 0; a < associativity; a++){
            cache_blocks[a].rp_value=cache_blocks[a].rp_value+1;

         }
         cache_blocks[pos_lru].rp_value = 0;
      }
      if(debug){
         print_way_info(idx,associativity,cache_blocks);
      }
      return OK;

   } else {
      if(debug){
         cout << "Se realiza un load " << endl;
      }
      //Se trae el dato a la posición libre y se actualiza el rp_value
      if(libre && !dato_presente){
         result->miss_hit = MISS_LOAD;

         if(debug){
            cout << "Posición libre: " << pos_libre << endl;
         }
         cache_blocks[pos_libre].tag = tag;
         cache_blocks[pos_libre].valid = true;
         cache_blocks[pos_libre].dirty = false;

         for(int a = 0; a < associativity; a++){
            if(cache_blocks[a].valid){
               cache_blocks[a].rp_value=cache_blocks[a].rp_value+1;

            }
         }
         cache_blocks[pos_libre].rp_value = 0;
      //Si no se tiene una posición libre, se reemplaza el dato con el valor mayor de LRU y se actualiza el rp_value
      } else if(!dato_presente){
         result->miss_hit = MISS_LOAD;

         if(debug){
            cout << "Posición del dato reemplazado: " << pos_lru << endl;
            cout << "Tag del dato reemplazado: " << cache_blocks[pos_lru].tag << endl;
            cout << "Bit de dirty: " << cache_blocks[pos_lru].dirty << endl;
         }
         result->dirty_eviction = cache_blocks[pos_lru].dirty;

         result->evicted_address = cache_blocks[pos_lru].tag;

         cache_blocks[pos_lru].tag = tag;
         cache_blocks[pos_lru].dirty = false;

         for(int a = 0; a < associativity; a++){
            cache_blocks[a].rp_value=cache_blocks[a].rp_value+1;

         }
         cache_blocks[pos_lru].rp_value = 0;
      }
      if(debug){
         print_way_info(idx,associativity,cache_blocks);
      }
      return OK;
   }

   return ERROR;
}

int nru_replacement_policy(int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug)
{

  bool freeSpace=false;
  int posLibre;
  bool hit=false;
  int posHit;
  bool primerUno=false;
  int posuno;

  if(idx < 0 || associativity <= 0 || tag < 0 || (ceil(log2(associativity)) != floor(log2(associativity)))){
     return PARAM;
  }

  if(debug){
     cout << "\nIniciando NRU: "<< endl;
  }



  for (int i = 0; i < associativity; i++) {
    if (cache_blocks[i].valid==false) { //Libre
      if (freeSpace==false) { //Primera posicion libre la guarda
        freeSpace=true;
        posLibre=i; //Aqui se almacena la primera posicion libre que encuentre

      }
    } //Si no es espacio libre
    else{
      if (cache_blocks[i].tag==tag) { //Hay un hit
        posHit=i; //Posicion donde hace el hit
        hit=true;
        if(debug){
           cout << "Se encontró el dato válido en la posición: " << posHit << endl;
        }
      }
      else { //No hay hit
        if (cache_blocks[i].rp_value==1 && !primerUno) { //Si encuentra el primer uno
          primerUno=true;
          posuno=i; //Primer uno disponible
        }
      }
    }
  }

  if (debug) {
    cout << "freespace " << freeSpace << " primerUno " << freeSpace << " hit " << hit << endl;
  }


    if (hit) { //Caso en el que hubo hit
      cache_blocks[posHit].rp_value=0;

      if(loadstore == true){ //true = store
        cache_blocks[posHit].dirty = true;
        operation_result->miss_hit = HIT_STORE;
        if(debug){
           cout << "Se realiza un store " << endl;
           cout << "Result miss_hit: " << operation_result->miss_hit << endl;
        }
      }
      else{ //false = load
        operation_result->miss_hit = HIT_LOAD;
        if(debug){
           cout << "Se realiza un load " << endl;
           cout << "Result miss_hit: " << operation_result->miss_hit << endl;
        }
      }
      if(debug){
         print_way_info(idx,associativity,cache_blocks);
      }
      return OK;
    }
    else{ //No hubo hit
      if (freeSpace) { //Hay posiciones libres en L1
        if(debug){
           cout << "Posición libre: " << posLibre << endl;
        }
        cache_blocks[posLibre].tag = tag;
        cache_blocks[posLibre].valid = true;
        cache_blocks[posLibre].rp_value = 0;
        if (loadstore) {
          cache_blocks[posLibre].dirty = true;
          operation_result->miss_hit = MISS_STORE;

        }
        else {
          cache_blocks[posLibre].dirty = false;
          operation_result->miss_hit = MISS_LOAD;
        }

      }

      else{ //no hubo hit y no hay posiciones libres
        if (!primerUno) { //En caso de que todos sean cero
          for (int i = 0; i < associativity; i++) { //llena todas las vias de 1
            cache_blocks[i].rp_value=1;
          }
          posuno=0; //La primera posicion para sustituir
        }


        if(debug){
           cout << "Posición del dato reemplazado: " << posuno << endl;
           cout << "Tag del dato reemplazado: " << cache_blocks[posuno].tag << endl;
           cout << "Bit de dirty: " << cache_blocks[posuno].dirty << endl;
        }
        operation_result->dirty_eviction = cache_blocks[posuno].dirty;
        //Cambiar el tag por el address
        operation_result->evicted_address = cache_blocks[posuno].tag;

        cache_blocks[posuno].tag = tag;
        cache_blocks[posuno].rp_value=0;
        if (loadstore) {
          cache_blocks[posuno].dirty = true;
          operation_result->miss_hit = MISS_STORE;
        }
        else {
          cache_blocks[posuno].dirty = false;
          operation_result->miss_hit = MISS_LOAD;
        }
      }
      if(debug){
         print_way_info(idx,associativity,cache_blocks);
      }
      return OK;
    }


    return ERROR;
  }


/* Get statistics */
void get_statistics(operation_result result , statistics* stat_result, int instruction_count) {

   switch(result.miss_hit) {
      case HIT_LOAD :
         stat_result->load_hits++;
         break;
      case MISS_LOAD :
         stat_result->load_misses++;
         break;
      case HIT_STORE :
         stat_result->store_hits++;
         break;
      case MISS_STORE:
         stat_result->store_misses++;
         break;
   }

   if (result.dirty_eviction) {
      stat_result->dirty_evictions++;
   }

   stat_result->instruction_count += instruction_count;
}
