/*
 *  Cache simulation project
 *  Class UCR IE-521
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define KB 1024
#define ADDRSIZE 32
#define LOAD false
#define STORE true
using namespace std;

int lru_obl_replacement_policy (int idx,
                                int tag,
                                int associativity,
                                bool loadstore,
                                entry* cache_block,
				                        entry* cache_block_obl,
                                operation_result* operation_result_cache_block,
				                        operation_result* operation_result_cache_obl,
                                bool debug=false)
{

   bool dato_presente = 0;
   int pos_libre = 0;
   bool libre = 0;
   int lru_max = 0;
   int pos_lru = 0;
   int pos_dato = 0;

   bool dato_presente_obl = 0;
   int pos_libre_obl = 0;
   bool libre_obl = 0;
   int lru_max_obl = 0;
   int pos_lru_obl = 0;
   int pos_dato_obl = 0;

   bool valor_obl = false;
   //Verificación de que los parámetros sean correctos
   if(idx < 0 || associativity <= 0 || tag < 0 || (ceil(log2(associativity)) != floor(log2(associativity)))){
      return PARAM;
   }
   if(debug){
      cout << "\nIniciando LRU_OBL: "<< endl;
   }
   //Se recorre cada entrada
   for(int i = 0; i < associativity; i++){
     //Se verifica si hay espacio vacío
      if(cache_block[i].valid == false){
         pos_libre = i;
         libre = 1;
      //Se verifica si hay un hit
      } else if(cache_block[i].tag == tag){
         dato_presente=true;
         pos_dato = i;
         if(debug){
            cout << "Se encontró el dato válido en la posición: " << pos_dato << endl;
         }
            if(loadstore){
               operation_result_cache_block->miss_hit = HIT_STORE;
               if(debug){
                  cout << "Result miss_hit: " << operation_result_cache_block->miss_hit << endl;
               }
            } else {
               operation_result_cache_block->miss_hit = HIT_LOAD;
               if(debug){
                  cout << "Result miss_hit: " << operation_result_cache_block->miss_hit << endl;
               }
            }
         //Se actualiza el valor de rp_value
         for(int a = 0; a < associativity; a++){
            if(cache_block[a].rp_value < cache_block[i].rp_value){
               cache_block[a].rp_value=cache_block[a].rp_value+1;
            }
         }
         cache_block[i].rp_value = 0;

         //Hay un hit en cache_block, ahora se verifica obl_tag para ver si se requiere hacer prefetch
         if(cache_block[i].obl_tag){
           cache_block[i].obl_tag = false;
           //Se indica que se debe hacer prefetch
           valor_obl = true;
         }
      //Se verifica el dato a ser reemplazado
      } else {
         if(cache_block[i].rp_value > lru_max){
            lru_max = cache_block[i].rp_value;
            pos_lru = i;
         }
      }
   }

   if(loadstore == STORE){

      if(debug){
         cout << "Se realiza un store " << endl;
      }
      //Si hay un hit se actualiza el dirty
      if(dato_presente){
         cache_block[pos_dato].dirty = true;
      //Si hay una posición libre se pone el dato en la posición correspondiente, y se actualiza el rp_value
      } else if(libre){
         operation_result_cache_block->miss_hit = MISS_STORE;

         if(debug){
            cout << "Posición libre: " << pos_libre << endl;
         }
         cache_block[pos_libre].dirty = true;
         cache_block[pos_libre].tag = tag;
         cache_block[pos_libre].valid = true;
         cache_block[pos_libre].obl_tag = false;
         valor_obl = true;

         for(int a = 0; a < associativity; a++){
            if(cache_block[a].valid){
               cache_block[a].rp_value=cache_block[a].rp_value+1;
            }
         }
         cache_block[pos_libre].rp_value = 0;
      //Si no se cumple alguna de las anteriores, se reemplaza el dato con el valor mayor de LRU y se actualiza el rp_value
      } else{
         operation_result_cache_block->miss_hit = MISS_STORE;

         if(debug){
            cout << "Posición del dato reemplazado: " << pos_lru << endl;
            cout << "Tag del dato reemplazado: " << cache_block[pos_lru].tag << endl;
            cout << "Bit de dirty: " << cache_block[pos_lru].dirty << endl;
         }
         operation_result_cache_block->dirty_eviction = cache_block[pos_lru].dirty;

         operation_result_cache_block->evicted_address = cache_block[pos_lru].tag;

         cache_block[pos_lru].dirty = true;
         cache_block[pos_lru].tag = tag;
         cache_block[pos_lru].obl_tag = false;
         valor_obl = true;

         for(int a = 0; a < associativity; a++){
            cache_block[a].rp_value=cache_block[a].rp_value+1;

         }
         cache_block[pos_lru].rp_value = 0;
      }
      if(debug){
         print_way_info(idx,associativity,cache_block);
      }
      if(!valor_obl){
        return OK;
      }
      

   } else {
      if(debug){
         cout << "Se realiza un load " << endl;
      }
      //Se trae el dato a la posición libre y se actualiza el rp_value
      if(libre && !dato_presente){
         operation_result_cache_block->miss_hit = MISS_LOAD;
         
         if(debug){
            cout << "Posición libre: " << pos_libre << endl;
         }
         cache_block[pos_libre].tag = tag;
         cache_block[pos_libre].valid = true;
         cache_block[pos_libre].dirty = false;
         cache_block[pos_libre].obl_tag = false;
         //Se indica que se debe hacer prefetch
         valor_obl = true;

         for(int a = 0; a < associativity; a++){
            if(cache_block[a].valid){
               cache_block[a].rp_value=cache_block[a].rp_value+1;

            }
         }
         cache_block[pos_libre].rp_value = 0;
      //Si no se tiene una posición libre, se reemplaza el dato con el valor mayor de LRU y se actualiza el rp_value
      } else if(!dato_presente){
         operation_result_cache_block->miss_hit = MISS_LOAD;

         if(debug){
            cout << "Posición del dato reemplazado: " << pos_lru << endl;
            cout << "Tag del dato reemplazado: " << cache_block[pos_lru].tag << endl;
            cout << "Bit de dirty: " << cache_block[pos_lru].dirty << endl;
         }
         operation_result_cache_block->dirty_eviction = cache_block[pos_lru].dirty;

         operation_result_cache_block->evicted_address = cache_block[pos_lru].tag;

         cache_block[pos_lru].tag = tag;
         cache_block[pos_lru].dirty = false;
         cache_block[pos_lru].obl_tag = false;

         //Se indica que se debe hacer prefetch
         valor_obl = true;

         for(int a = 0; a < associativity; a++){
            cache_block[a].rp_value=cache_block[a].rp_value+1;

         }
         cache_block[pos_lru].rp_value = 0;
      }
      if(debug){
         print_way_info(idx,associativity,cache_block);
      }
      if(!valor_obl){
        return OK;
      }
   }

  //Se verifica si se tiene que hacer prefetch
  if(valor_obl){
    //Se recorre cada entrada del segundo set
    for(int i = 0; i < associativity; i++){
      //Se verifica si hay espacio vacío
        if(cache_block_obl[i].valid == false){
          pos_libre_obl = i;
          libre_obl = 1;
        //Se verifica si hay un hit
        } else if(cache_block_obl[i].tag == tag){
          dato_presente_obl=true;
          pos_dato_obl = i;
          if(debug){
              cout << "Se encontró el dato de prefetch válido en la posición: " << pos_dato_obl << endl;
          }
        //Se verifica el dato a ser reemplazado
        } else {
          if(cache_block_obl[i].rp_value > lru_max_obl){
              lru_max_obl = cache_block_obl[i].rp_value;
              pos_lru_obl = i;
          }
        }
    }

    if(debug){
      cout << "Se realiza el prefetch " << endl;
    }
    //Se trae el dato a la posición libre y se actualiza el rp_value
    if(libre_obl && !dato_presente_obl){
      operation_result_cache_obl->miss_hit = MISS_LOAD;
      
      if(debug){
          cout << "Posición libre_obl: " << pos_libre_obl << endl;
      }
      cache_block_obl[pos_libre_obl].tag = tag;
      cache_block_obl[pos_libre_obl].valid = true;
      cache_block_obl[pos_libre_obl].obl_tag = true;
      cache_block_obl[pos_libre_obl].dirty = false;
      for(int a = 0; a < associativity; a++){
          if(cache_block_obl[a].valid){
            cache_block_obl[a].rp_value=cache_block_obl[a].rp_value+1;
          }
      }
      cache_block_obl[pos_libre_obl].rp_value = 0;
      //Si no se tiene una posición libre, se reemplaza el dato con el valor mayor de LRU y se actualiza el rp_value
    } else if(!dato_presente_obl){
      operation_result_cache_obl->miss_hit = MISS_LOAD;
      
      if(debug){
          cout << "Posición del dato reemplazado: " << pos_lru_obl << endl;
          cout << "Tag del dato reemplazado: " << cache_block_obl[pos_lru_obl].tag << endl;
          cout << "Bit de dirty: " << cache_block_obl[pos_lru_obl].dirty << endl;
      }
      operation_result_cache_obl->dirty_eviction = cache_block_obl[pos_lru_obl].dirty;
      operation_result_cache_obl->evicted_address = cache_block_obl[pos_lru_obl].tag;

      cache_block_obl[pos_lru_obl].tag = tag;
      cache_block_obl[pos_lru_obl].obl_tag = true;
      cache_block_obl[pos_lru_obl].dirty = false;
      for(int a = 0; a < associativity; a++){
          cache_block_obl[a].rp_value=cache_block_obl[a].rp_value+1;
      }
      cache_block_obl[pos_lru_obl].rp_value = 0;
    }
    if(debug){
      print_way_info(idx,associativity,cache_block_obl);
    }
    return OK;
  }

   return ERROR;
}
